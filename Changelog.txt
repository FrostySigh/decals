*This list is not complete there are more things I've added but not written down.

///////////////////////////
v10:
///////////////////////////
Added: Custom UNSC Logo Alpha
Added: BR55 Warning Lable Alpha
Added: BR55 Screen Alpha
Added: BR55HB Logo Alpha
Added: BR55 Logo Alpha
Added: MA5 Series Logo Alpha
Added: Mars Export Alpha
Added: Viper 12 & 8 Guage Ammo Alpha
Added: Magazine Match Alpha
Added: Eject Alpha
Added: Push Alpha
Added: FC UNSC Alpha
Added: Chalybs Simplified Alpha
Added: Ushuaia Armory Alpha
Added: Vakara GmbH Alpha
Added: Sinoviet Logo Simplified Alpha
Added: Optican Alpha
///////////////////////////
v9:
///////////////////////////
Added: UNSC with border Alpha
Added: W/AV M6 G GNR Alpha
Added: SPNKr with border Alpha
Added: SPNKr Rocket Icon Alpha
Added: 8Gauge Hippoo Logo Alpha
Added: 12Gauge Hippoo Logo Alpha
Added: Battery Icon Alpha
Added: Emerson Tactical Systems Logo Alpha
Added: FUD UNSC Logo Alpha

///////////////////////////
v8:
///////////////////////////
Added: Lethbridge Industrial logo Alpha
Added: Lethbridge Industrial Simple Alpha
Added: Industrial Decal Sheet (Requested by Forky)

///////////////////////////
v7:
///////////////////////////
Added: SPNKr Name Icon Alpha
Added: G4H-DuSH Icon Alpha
Added: G4H-DuSH Name Alpha
Added: Colonial Police Department Alpha
Added: Scorpion Logo Alpha
Changed: Moved the Weapons Specific ones to the main folder

///////////////////////////
v6:
///////////////////////////
Added: Magazine Icon
Added: Medical Symbol
Added: Tie Down Vehicle Text
Added: Safe, Fire, Burst, Auto and Semi Weapon alphas
Added: Misriah vehicle/weapon text
Added: Arrow icon thingy
Added: Arrow 2 Icon thingy

///////////////////////////
v5:
///////////////////////////
Added: Numbers 0-9 (Vehicle Specific > Numbers)
Added: Spartan Laser Decal (Weapon Specific)
Added: Fueder Construction (Civilian)
Added: Turbogen Decal


///////////////////////////
v4:
///////////////////////////
Added: Watch Step Decal (With Alternate font)


///////////////////////////
v3:
///////////////////////////
Changed: File structure now added categories (Civilian, UEG, Vehicle Specific and Military Manufacturer)

---------------------------

Added: UNSC Airforce Logo (Fanmade by Frosty with input from the community and the other Devs)
Added: UNSC Airwing Logo (Based on the Pilot Helmet Logo but my own spin on it)
Added: UNSC Marine Corps Logo (Seen on hornets in Halo 3)
Added: Armory Decal (With Alternate font)
Added: Caution Decal (With Alternate font)
Added: Danger Decal (With Alternate font)
Added: Galilean Ammo Decal
Added: M/AAV-20 Decal (With Alternate font)
Added: No Step Decal (With Alternate font)
Added: Step Decal (With Alternate font)
Added: To Fold Decal (With Alternate font)
Added: UNSC Decal Alt. Font


///////////////////////////
v2:
///////////////////////////
Added: Chalybs Defense Solutions Logo
Added: Asklon Logo
Added: AMG Transport Dynamics Logo


///////////////////////////
v1:
///////////////////////////
Added: UEG Logo
Added: UNSC Logo (Pre war)
Added: BXR Logo
Added: Forge Logo
Added: Hannibal Weapon Systems Logo
Added: Marathon Logo
Added: Jotun Heavy Industries Logo
Added: Mirisah Armory Logo
Added: Naphtali Contractor Corporation Logo
Added: ONI Logo
Added: Sinoviet Heavy Machinery Logo
Added: Traxus Heavy Industries Logo
Added: New Mombasa Police Department Logo
Added: Colonial Administration Authority Logo
Added: UNSC Name Logo
Added: UNSC Army Logo
Added: Project: ORION Logo
Added: UNSC Stylized name Logo (Possibly post war?)
Added: ODST Logo 
Added: Corbulo Acadamy Logo
Added: UNSC Navy Logo
Added: Spartan II Program Logo
Added: Spartan III Program Logo (Possibly the wrong one, couldn't find a proper one but this one makes sense)