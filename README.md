# What is this?

Here are pretty much all the Halo related decals I've made over the years and projects I've been apart of, I'm making these available to use since they're just collecting dust currently, I only ask for some sort of credit if they're used.


# Disclaimer

I didn't come up with these designs they're probably the property of Microsoft and 343 Industries/Bungie except for the fan-made versions of the UNSC Air Force, Wet Navy and Airwing logos. 

*If someone wants to add more decals to this repository please contact me on discord: FrostySigh*